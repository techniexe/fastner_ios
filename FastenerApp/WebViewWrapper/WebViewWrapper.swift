//
//  WebViewWrapper.swift
//  FastenerApp
//
//  Created by Techniexe Infolabs on 15/12/20.
//

import UIKit
import WebKit

class WebViewWrapper: WKWebView {
  
  required init?(coder: NSCoder) {
    let configuration = WKWebViewConfiguration()
    let controller = WKUserContentController()
    configuration.userContentController = controller
    super.init(frame: CGRect.zero, configuration: configuration)
  }
  
  /*
   // Only override draw() if you perform custom drawing.
   // An empty implementation adversely affects performance during animation.
   override func draw(_ rect: CGRect) {
   // Drawing code
   }
   */
  
}
