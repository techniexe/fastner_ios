//
//  ViewController.swift
//  FastenerApp
//
//  Created by Techniexe Infolabs on 11/12/20.
//

import UIKit
import WebKit

class ViewController: UIViewController {

  // MARK: - @IBOutlets
  @IBOutlet weak var webView: WebViewWrapper!
  var zoomEnabled: Bool = false
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
    // Do any additional setup after loading the view.
  }

  // MARK: - setupView
  func setupView() {
    
    webView.navigationDelegate = self
    
    if #available(iOS 13.0, *) {
      activityIndicator.style = .large
    } else {
      activityIndicator.style = .whiteLarge
    }
    
    // webView.configuration.userContentController.add(self, name: "JavaScriptObserver")
    
    self.zoomDisable()
    
    
    //    progressView = UIProgressView(progressViewStyle: .default)
    //    progressView.sizeToFit()
    //    objWebView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
    //    self.activityIndicator.startAnimating()
    //    self.callRequestAPI()
    
    //    objWebView.navigationDelegate = self
    //
    //    let url = URL(string: "https://www.hackingwithswift.com")!
    //    objWebView.load(URLRequest(url: url))
    //    objWebView.allowsBackForwardNavigationGestures = true
  }
  
  // MARK: - Disable Zoom in WebView
  func zoomDisable() {
    // Disable zoom in web view
    let source: String = "var meta = document.createElement('meta');" +
      "meta.name = 'viewport';" +
      "meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';" +
      "var head = document.getElementsByTagName('head')[0];" + "head.appendChild(meta);"
    let script: WKUserScript = WKUserScript(source: source, injectionTime: .atDocumentEnd,
                                            forMainFrameOnly: true)
    webView.configuration.userContentController.addUserScript(script)
    webView.reload()
    
    self.loadWebViewContent()
  }
  
  // MARK: - Load WebView Content
  private func loadWebViewContent(_ file: String = "index", asFile: Bool = true) {
    //
      //let request = URLRequest(url: URL(string: "https://onlinefreshfoods.com/")!)
    
      //webView.load(request)
    
    
    guard let filePath = Bundle.main.path(forResource: file, ofType: "html",
                                          inDirectory:  "LocalWebAssets") else {
      Helper.DLog(message: "Unable to load local html file: \(file)")
      return
    }
    
    if asFile {
      // load local file
      let filePathURL = URL.init(fileURLWithPath: filePath)
      let fileDirectoryURL = filePathURL.deletingLastPathComponent()
      webView.loadFileURL(filePathURL, allowingReadAccessTo: fileDirectoryURL)
      
    } else {
      do {
        // load html string. baseURL is required for local files to load correctly
        let html = try String(contentsOfFile: filePath, encoding: .utf8)
        webView.loadHTMLString(html, baseURL: Bundle.main.resourceURL?.appendingPathComponent("LocalWebAssets"))
      } catch {
        Helper.DLog(message: "Unable to load local html resource as string")
      }
    }
  }
  
  
//  // MARK: - Call Request API
//  func callRequestAPI() {
//    objWebView.navigationDelegate = self
//    objWebView.uiDelegate = self
//    objWebView.scrollView.delegate  = self
//    let request = URLRequest(url: URL(string: "https://www.hackingwithswift.com")!)
//    objWebView?.load(request)
//    objWebView.sizeToFit()
//  }
//
//  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//      if keyPath == "estimatedProgress" {
//          progressView.progress = Float(webView.estimatedProgress)
//      }
//  }
}

// MARK: - UIScrollViewDelegate
//extension ViewController: UIScrollViewDelegate {
//
//  func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
//    scrollView.pinchGestureRecognizer?.isEnabled = false
//  }
//}

//// MARK: - WKNavigationDelegate, WKUIDelegate
//extension ViewController: WKNavigationDelegate, WKUIDelegate {
//
//  func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//    self.activityIndicator.startAnimating()
//  }
//
//  func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
//
//  }
//
//  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//    self.activityIndicator.stopAnimating()
//  }
//
//  func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
//    self.activityIndicator.stopAnimating()
//  }
//
//  func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
//    self.activityIndicator.stopAnimating()
//  }
//
//}



// MARK: - WKNavigationDelegate
extension ViewController : WKNavigationDelegate {
  
  func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    self.activityIndicator.startAnimating()
  }
  
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    self.activityIndicator.stopAnimating()
  }
  
  func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
    self.activityIndicator.stopAnimating()
  }
  
  func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
    self.activityIndicator.stopAnimating()
  }
  
  func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
    //decisionHandler(.allow)
    //return
    if navigationAction.navigationType == .linkActivated {
      guard let url = navigationAction.request.url else {
        print("Link is not a url")
        decisionHandler(.allow)
        return
      }
      if url.absoluteString.hasPrefix("file:") {
        print("Open link locally")
        decisionHandler(.allow)
      }else if let host = url.host,
               !host.hasPrefix("svasilevkin.wordpress.com"),
               UIApplication.shared.canOpenURL(url) {
        UIApplication.shared.open(url)
        print(url)
        print("Redirected to browser.")
        decisionHandler(.cancel)
      }else if url.absoluteString.hasPrefix("mailto:") {
        print("Send email locally")
        //sendEmail()
        decisionHandler(.allow)
      } else {
        print("Open link locally")
        decisionHandler(.allow)
      }
    } else {
      print("not a user click")
      decisionHandler(.allow)
    }
  }
}


//// MARK: - WKScriptMessageHandler
//extension ViewController : WKScriptMessageHandler {
//  func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
//    // Callback from JavaScript:
//    // window.webkit.messageHandlers.JavaScriptObserver.postMessage(message)
//    let text = message.body as? String
//    let alertController = UIAlertController(title: "Message from JavaScript",
//                                            message: text, preferredStyle: .alert)
//    let okAction = UIAlertAction(title: "OK", style: .default) { action in
//      print("OK")
//    }
//    alertController.addAction(okAction)
//    present(alertController, animated: true, completion: nil)
//  }
//}
