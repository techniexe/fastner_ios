//
//  Helper.swift
//  FastenerApp
//
//  Created by Techniexe Infolabs on 15/12/20.
//

import Foundation

class Helper {
  
  // MARK: - Print
  static func DLog(message: String, function: String = #function) {
    #if DEBUG
    print("\(function): \(message)")
    #endif
  }
  
  // MARK: - Check key is perent or not
  static func isKeyPresentInUserDefaults(key: String) -> Bool {
    return UserDefaults.standard.object(forKey: key) != nil
  }
  
}
