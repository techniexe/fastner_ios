//
//  Constants.swift
//  FastenerApp
//
//  Created by Techniexe Infolabs on 15/12/20.
//

import Foundation
import UIKit

struct Constants {
  
  static let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
  
  struct Application {
    static let name = "FastenerApp"
  }
  
}

